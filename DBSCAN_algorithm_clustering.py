import pandas as pd
from pylab import rcParams
import seaborn as sb
import matplotlib.pyplot as plt
import Dcluster as dcl
import numpy as np

from sklearn.cluster import DBSCAN
from mpl_toolkits.mplot3d import Axes3D
from collections import Counter

import sklearn.neighbors as skn

rcParams['figure.figsize'] = 5, 4
sb.set_style('whitegrid')

columnNames = ['Hue_1', 'Sat_1', 'Val_1' , 'Image_FileName']
df = pd.read_csv(filepath_or_buffer='Colors - Copy.csv', header=None, names=columnNames)

data = df.iloc[:, 0:2]
target = df.iloc[:, 6]

epsilon_list = []
min_samples_list = []
lables_list = []
outliers_list = []

# To detect best fit for eps and min_samples

for i in range(20):
    for j in range(100):
        model = DBSCAN(eps=i + 1, min_samples=j + 1).fit(data)
        if 8 < max(model.labels_) <= 33:
            epsilon_list.append(i + 1)
            min_samples_list.append(j + 1)
            lables_list.append(max(model.labels_))
            outliers_list.append(Counter(model.labels_ == -1)[True])

DBSCAN_model_params = pd.DataFrame(np.column_stack([epsilon_list, min_samples_list, lables_list, outliers_list]),
                                   columns=['Epsilon', 'Cluster sample', 'labels', 'Outliers'])

with open('DBSCAN_model_params_new.csv', 'a') as f:
    pd.DataFrame.drop_duplicates(DBSCAN_model_params).to_csv(f, index=False)

# min_samples_list = [j + 1]

model = DBSCAN(eps=2, min_samples=26).fit(data)

print(max(model.labels_))
data = pd.DataFrame(data)
colors = model.labels_

fig = plt.figure()
print('Records that are assigned me with the respective lables')
print(Counter(model.labels_))

data.plot(kind='scatter', x='Hue_1', y='Sat_1', c=colors)

plt.title('DBSCAN clustering pills')

plt.show()
