import numpy as np
import seaborn as sb
from sklearn.cluster import DBSCAN

import matplotlib.pyplot as plt
import pandas as pd

model_params = pd.read_csv('DBSCAN_model_params.csv', header=None)
model_params.columns = ['Epsilon','Cluster sample','labels','Outliers']

plt.plot(model_params['Cluster sample'],model_params['labels'])
#plt.plot(model_params['Epsilon'],model_params['Outliers'])
plt.show()
