import numpy as np
import matplotlib.pyplot as plt
import pandas
import sklearn
from sklearn.cluster import DBSCAN
from collections import Counter

filename = 'train.csv'

columnNames = ['Hue_1', 'Sat_1', 'Val_1', 'Hue_2', 'Sat_2', 'Val_2', 'Color ID', 'Image_FileName']
data_raw = pandas.read_csv(filename, names=columnNames)

data = data_raw[['Hue_1', 'Sat_1', 'Val_1', 'Color ID']]
df = pandas.DataFrame(data)

print(df.describe())
print('\n')

class_counts = df.groupby('Color ID').size()

print(class_counts)
print('\n')

types = df.dtypes
print(types)
print('\n')

grouped = df.groupby('Color ID')

print(grouped.describe())
print('\n')

for name, group in grouped:
    print('ID: ' + str(name))
    group_new = group[['Hue_1', 'Sat_1', 'Val_1']]
    _, bp = pandas.DataFrame.boxplot(group_new, return_type='both')
    outliers = [flier.get_ydata() for flier in bp["fliers"]]
    boxes = [box.get_ydata() for box in bp["boxes"]]
    medians = [median.get_ydata() for median in bp["medians"]]
    whiskers = [whiskers.get_ydata() for whiskers in bp["whiskers"]]
    print('grouped outlier for ' + str(name))
    print('/n')
    outlier_Hue_1 = list(set(np.ndarray.tolist(outliers[0])))
    outlier_Sat_1 = list(set(np.ndarray.tolist(outliers[1])))
    outlier_Val_1 = list(set(np.ndarray.tolist(outliers[2])))

    with open('outliers_union.csv', 'a') as f:
        outliers_union = pandas.concat(
            [data_raw[data_raw.Hue_1.isin(outlier_Hue_1)], data_raw[data_raw.Sat_1.isin(outlier_Sat_1)],
             data_raw[data_raw.Val_1.isin(outlier_Val_1)]])
        pandas.DataFrame.drop_duplicates(outliers_union).to_csv(f, header=False, index=False)

    with open('outliers_union_hue_sat.csv', 'a') as a:
        outliers_union_hue_sat = pandas.concat(
            [data_raw[data_raw.Hue_1.isin(outlier_Hue_1)], data_raw[data_raw.Sat_1.isin(outlier_Sat_1)]])
        pandas.DataFrame.drop_duplicates(outliers_union_hue_sat).to_csv(a, header=False, index=False)

    with open('outliers_Hue.csv', 'a') as g:
        outliers_Hue = pandas.DataFrame.drop_duplicates(data_raw[data_raw.Hue_1.isin(outlier_Hue_1)])
        outliers_Hue.to_csv(g, header=False,
                            index=False)

    with open('outliers_Sat.csv', 'a') as h:
        outliers_sat = pandas.DataFrame.drop_duplicates(data_raw[data_raw.Sat_1.isin(outlier_Sat_1)])
        outliers_sat.to_csv(h, header=False,
                            index=False)

    with open('outliers_Val.csv', 'a') as i:
        outlier_Val = pandas.DataFrame.drop_duplicates(data_raw[data_raw.Val_1.isin(outlier_Val_1)])
        outlier_Val.to_csv(i, header=False,
                           index=False)

    outliers_intersection_hue_sat = pandas.merge(data_raw[data_raw.Hue_1.isin(outlier_Hue_1)],
                                                 data_raw[data_raw.Sat_1.isin(outlier_Sat_1)], how='inner')

    outliers_intersection = pandas.merge(outliers_intersection_hue_sat, data_raw[data_raw.Val_1.isin(outlier_Val_1)],
                                         how='inner')

    with open('outliers_intersection_hue_sat.csv', 'a') as j:
        outliers_intersection_hue_sat.to_csv(j, header=False, index=False)

    with open('outliers_intersection.csv', 'a') as k:
        outliers_intersection.to_csv(k, header=False, index=False)

    print('----------------------------')
    print('\n')


plt.show()
