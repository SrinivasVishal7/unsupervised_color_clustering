import numpy as np
import matplotlib.pyplot as plt
import pandas
from scipy import stats
import seaborn

filename = 'train.csv'


# raw_data = open(filename,'rt')

# raw_data = urllib.urlopen('https://goo.gl/vhm1eU')
# data = numpy.loadtxt(raw_data,delimiter=',')
# print(data.shape)

def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3 * std
    group[outliers] = mean  # or "group[~outliers].mean()"
    return group


columnNames = ['Hue_1', 'Sat_1', 'Val_1', 'Hue_2', 'Sat_2', 'Val_2', 'Color ID', 'File_Name']
data = pandas.read_csv(filename, names=columnNames)

data = data[['Hue_1', 'Sat_1', 'Val_1', 'Color ID']]
df = pandas.DataFrame(data)

print(df.describe())
print('\n')

class_counts = df.groupby('Color ID').size()

print(class_counts)
print('\n')

types = df.dtypes
print(types)
print('\n')

grouped = df.groupby('Color ID')

print(grouped.describe())
print('\n')

# grouped.plot(kind='box')

for name, group in grouped:
    print('ID: ' + str(name))
    group_new = group[['Hue_1', 'Sat_1', 'Val_1','File_Name']]

    # q75_Hue, q25_Hue = np.percentile(group_new.dropna(), [75, 25])
    # iqr = q75_Hue - q25_Hue
    # min = q25_Hue - (iqr * 1.5)
    # max = q75_Hue + (iqr * 1.5)

    # fig, axes = plt.subplots(nrows=1, ncols=2)
    # axes[0].boxplot(group_new)
    grouped_outlier = group_new[(np.abs(stats.zscore(group_new)) > 3)]
    # grouped_outlier = group_new[group_new.apply(lambda x: np.abs(x - x.mean()) / x.std() > 3).all(axis=1)]
    # grouped_outlier_Hue1 = group_new[
    #     np.abs(group_new[['Hue_1']] - group_new[['Hue_1']].mean()) > (3 * group_new[['Hue_1']].std())]
    # grouped_outlier_Sat1 = group_new[
    #     np.abs(group_new[['Sat_1']] - group_new[['Sat_1']].mean()) > (3 * group_new[['Sat_1']].std())]
    # grouped_outlier_Val1 = group_new[
    #     np.abs(group_new[['Val_1']] - group_new[['Val_1']].mean()) > (3 * group_new[['Val_1']].std())]
    print('grouped outlier for ' + str(name))
    print(grouped_outlier)
    print('/n')
    print(grouped_outlier.describe())
    # if not grouped_outlier.empty:
    #     grouped_outlier.plot(kind='box')
    print('----------------------------')
    print('\n')

# stats.winsorize(df)
plt.show()

# grouped.plot(kind='box')

# grouped_new = grouped_new[(np.abs(stats.zscore(grouped)) < 3).all(axis=1)]

# grouped_new.plot(kind='box')

# rowlength = grouped.ngroups/2
# fig, axs = plt.plot(8)

# print(fig)
# print(axs)
# print('\n')
#
# targets = zip(grouped.groups.keys(), axs.flatten())
# print(targets)
#
# for (key, ax) in enumerate(targets):
#         ax.boxplot(grouped.get_group(key))
#         ax.set_title('Color ID =%d' %key)
#
#
# ax.legend(['Hue_1','Sat_1','Val_1'])


# fig = plt.figure()
# ax = fig.add_subplot(111)
# cax = ax.matshow(data.corr(method='pearson'),vmin=-1,vmax=1)
# fig.colorbar(cax)
# pandas.plotting.scatter_matrix(data)
# ax.set_xticklabels(columnNames)
# ax.set_yticklabels(columnNames)
# plt.show()


# reader = csv.reader(raw_data,delimiter=',',quoting=csv.QUOTE_NONE)
# x = list(reader)
# data = numpy.array(x).astype(float)
# print(data.shape)
