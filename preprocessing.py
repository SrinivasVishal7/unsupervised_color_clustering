import numpy
import matplotlib.pyplot as plt
import pandas
import csv
import urllib
import sklearn
from sklearn.preprocessing import StandardScaler

filename = 'pima-indians-diabetes.csv'
columnNames = ['No.preg','plasma','pressure','triceps','insulin','BMI','pedi','age','class']
dataframe = pandas.read_csv(filename,names=columnNames)
array = dataframe.values
input_Data = array[:,0:8]
output_Data = array[:,8]
scaler = StandardScaler().fit(input_Data)
rescaled_input = scaler.transform(input_Data)
numpy.set_printoptions(precision=3)
print(rescaled_input[0:5,:])

