import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

import sklearn
from matplotlib.axes import rcParams
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import scale

import sklearn.metrics as sm
from sklearn import datasets
from sklearn.metrics import confusion_matrix, classification_report

columnNames = ['Hue_1', 'Sat_1', 'Val_1', 'Image_FileName']
df = pd.read_csv(filepath_or_buffer='Colors - Copy.csv', header=None, names=columnNames)

data = df.iloc[:, 0:2]

X = data.as_matrix()
X = scale(X)
variable_names = ['Hue_1', 'Sat_1']

clustering = KMeans()
clustering.fit(X)

plt.subplot(1, 1, 1)

plt.scatter(x=data.Hue_1, y=data.Sat_1,  c=clustering.labels_)


plt.title('Clustering prediction plot')

plt.xlabel('Hue_1')

print clustering

print clustering.labels_

plt.show()

se = pd.Series(clustering.labels_)

df['New_lables'] = se

with open('full_labeled.csv', 'a') as g:
    df.to_csv(g, index=False)

df_grouped = df.groupby('New_lables')
for name, df_grouped in df_grouped:
    print ("\nLabel : {} , Count for each labels : {}".format(name, len(df_grouped.index)))
    print(df_grouped.describe())
    with open('{}.csv'.format(name), 'a') as f:
        df_grouped.to_csv(f, index=False)


